package model;

public class Direttore extends Persona{
	
	private int eta;

	public int getEta() {
		return eta;
	}
	public void setEta(int eta) {
		this.eta = eta;
	}

	public Direttore(String codiceFiscale, String indirizzo, String telefono, int eta) {
		super(codiceFiscale, indirizzo, telefono);
		this.eta = eta;

	}
	
	

}
