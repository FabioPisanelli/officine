package model;

public class Veicolo {

	private String modello;
	private String tipo;
	private String targa;
	private int annoImmatricolazione;
	private Persona proprietario;
	
	public Persona getProprietario() {
		return proprietario;
	}
	public void setProprietario(Persona proprietario) {
		this.proprietario = proprietario;
	}
	public String getModello() {
		return modello;
	}
	public String getTipo() {
		return tipo;
	}
	public String getTarga() {
		return targa;
	}
	public int getAnnoImmatricolazione() {
		return annoImmatricolazione;
	}
	public Veicolo(String modello, String tipo, String targa, int annoImmatricolazione, Persona proprietario) {
		this.modello = modello;
		this.tipo = tipo;
		this.targa = targa;
		this.annoImmatricolazione = annoImmatricolazione;
		this.proprietario = proprietario;
	}
	
	@Override
	public String toString() {
		return "Veicolo [modello=" + modello + ", tipo=" + tipo + ", targa=" + targa + ", annoImmatricolazione="
				+ annoImmatricolazione + ", proprietario=" + proprietario + "]";
	}
	
	
	
	
	
	
	
}
