package model;

import java.util.Date;

public class ContrattoDipendente extends Contratto{


	private Dipendente dipendente;

	public Dipendente getDipendente() {
		return dipendente;
	}
	public ContrattoDipendente(Dipendente dipendente, Officina officina, Date dataInizio, Date dataFine) {
		super(officina, dataInizio, dataFine);
		this.dipendente = dipendente;
	}
	@Override
	public String toString() {
		return "ContrattoDipendente [dipendente=" + dipendente + "]";
	}

	



	


	
	
	
}
