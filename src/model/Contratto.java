package model;

import java.util.Date;

public class Contratto {

	private Officina officina;
	private Date dataInizio;
	private Date dataFine = null;
	
	public Date getDataFine() {
		return dataFine;
	}
	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}
	public Officina getOfficina() {
		return officina;
	}
	public Date getDataInizio() {
		return dataInizio;
	}
	public Contratto(Officina officina, Date dataInizio, Date dataFine) {
		this.officina = officina;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
	}
	
	@Override
	public String toString() {
		return "Contratto [officina=" + officina + ", dataInizio=" + dataInizio + ", dataFine=" + dataFine + "]";
	}
	
	
	
}
