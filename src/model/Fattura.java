package model;

import java.util.ArrayList;
import java.util.Date;

public class Fattura {

	private int numero;
	private Date dataRilascio;
	private ArrayList<Dipendente> elencoDipendentiLavorato;
	private Persona compilatore;
	private double costo;
	
	public Date getDataRilascio() {
		return dataRilascio;
	}
	public ArrayList<Dipendente> getElencoDipendentiLavorato() {
		return elencoDipendentiLavorato;
	}
	public Persona getCompilatore() {
		return compilatore;
	}
	public double getCosto() {
		return costo;
	}
	public int getNumero() {
		return numero;
	}
	
	public Fattura(int numero, Date dataRilascio, ArrayList<Dipendente> elencoDipendentiLavorato, Persona compilatore,
			double costo) {
		this.numero = numero;
		this.dataRilascio = dataRilascio;
		this.elencoDipendentiLavorato = elencoDipendentiLavorato;
		this.compilatore = compilatore;
		this.costo = costo;
	}
	@Override
	public String toString() {
		return "Fattura [numero=" + numero + ", dataRilascio=" + dataRilascio + ", elencoDipendentiLavorato="
				+ elencoDipendentiLavorato + ", compilatore=" + compilatore + ", costo=" + costo + "]";
	}
	
	
	
}
