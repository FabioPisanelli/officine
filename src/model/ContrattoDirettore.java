package model;

import java.util.Date;

public class ContrattoDirettore extends Contratto{


	private Direttore direttore;
	
	public Direttore getDirettore() {
		return direttore;
	}

	public ContrattoDirettore(Direttore direttore, Officina officina, Date dataInizio, Date dataFine) {
		super(officina, dataInizio, dataFine);
	}

	@Override
	public String toString() {
		return "ContrattoDirettore [direttore=" + direttore + "]";
	}

	

	
	
	
	
	
	
	
}
