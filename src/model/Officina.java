package model;

public class Officina {

	private String nome;
	private String indirizzo;
	private Direttore direttore;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public Direttore getDirettore() {
		return direttore;
	}
	public void setDirettore(Direttore direttore) {
		this.direttore = direttore;
	}
	public Officina(String nome, String indirizzo, Direttore direttore) {
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.direttore = direttore;
	}
	@Override
	public String toString() {
		return "Officina [nome=" + nome + ", indirizzo=" + indirizzo + ", direttore=" + direttore + "]";
	}
	
	
	
}
