package model;

import java.util.Date;

public class Riparazione {

	private int codiceRiparazione;
	private Officina officina;
	private Veicolo veicolo;
	private Date dataAccettazione;
	private Fattura fattura;
	
	public int getCodiceRiparazione() {
		return codiceRiparazione;
	}
	public Officina getOfficina() {
		return officina;
	}
	public Veicolo getVeicolo() {
		return veicolo;
	}
	public Date getDataAccettazione() {
		return dataAccettazione;
	}
	public Fattura getFattura() {
		return fattura;
	}
	
	
	public Riparazione(int codiceRiparazione, Officina officina, Veicolo veicolo, Date dataAccettazione,
			Fattura fattura) {
		this.codiceRiparazione = codiceRiparazione;
		this.officina = officina;
		this.veicolo = veicolo;
		this.dataAccettazione = dataAccettazione;
		this.fattura = fattura;
	}
	@Override
	public String toString() {
		return "Riparazione [codiceRiparazione=" + codiceRiparazione + ", officina=" + officina + ", veicolo=" + veicolo
				+ ", dataAccettazione=" + dataAccettazione + ", fattura=" + fattura + "]";
	}
	
	
	
	
}
