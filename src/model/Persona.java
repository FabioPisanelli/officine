package model;

public class Persona {

	private String codiceFiscale;
	private String indirizzo;
	private String telefono;
	
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public Persona(String codiceFiscale, String indirizzo, String telefono) {
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.telefono = telefono;
	}
	@Override
	public String toString() {
		return "Persona [codiceFiscale=" + codiceFiscale + ", indirizzo=" + indirizzo + ", telefono=" + telefono + "]";
	}
	
	
	
}
