package controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import model.ContrattoDipendente;
import model.ContrattoDirettore;
import model.Dipendente;
import model.Direttore;
import model.Fattura;
import model.Officina;
import model.Persona;
import model.Riparazione;
import model.Veicolo;

public class ControllerOfficine {

	//TUTTI GLI ARRAY
	private ArrayList<Persona> listaPersone = new ArrayList<Persona>();
	private ArrayList<Direttore> listaDirettori = new ArrayList<Direttore>();
	private ArrayList<Dipendente> listaDipendenti = new ArrayList<Dipendente>();
	private ArrayList<Veicolo> listaVeicoli = new ArrayList<Veicolo>();
	private ArrayList<Officina> listaOfficine = new ArrayList<Officina>();
	private ArrayList<ContrattoDirettore> listaContrattiDirettori = new ArrayList<ContrattoDirettore>();
	private ArrayList<ContrattoDipendente> listaContrattiDipendenti = new ArrayList<ContrattoDipendente>();
	private ArrayList<Fattura> listaFatture = new ArrayList<Fattura>();
	private ArrayList<Riparazione> listaRiparazioni = new ArrayList<Riparazione>();
	
	
	//TUTTI I METODI GETALL DELLE LISTE 
	public ArrayList<Persona> getAllPersone(){
		return listaPersone;
	};
	public ArrayList<Direttore> getAllDirettori(){
		return listaDirettori;
	};
	public ArrayList<Dipendente> getAllDipendenti(){
		return listaDipendenti;
	};
	public ArrayList<Veicolo> getAllVeicoli(){
		return listaVeicoli;
	};
	public ArrayList<Officina> getAllOfficine(){
		return listaOfficine;
	};
	public ArrayList<ContrattoDirettore> getAllContrattiDirettori(){
		return listaContrattiDirettori;
	};
	public ArrayList<ContrattoDipendente> getAllContrattiDipendenti(){
		return listaContrattiDipendenti;
	};
	public ArrayList<Fattura> getAllFatture(){
		return listaFatture;
	};
	public ArrayList<Riparazione> getAllRiparazioni(){
		return listaRiparazioni;
	};
	
	//METODI GET BY PER I SINGOLI IN LISTE
	public Persona getPersonaByCF(String codiceFiscale) {
		Persona persona = null;
		for(Persona p : listaPersone) {
			if(p.getCodiceFiscale().equals(codiceFiscale)) {
				persona = p;
			}
		}
		return persona;
	}
	public Direttore getDirettoreByCF(String codiceFiscale) {
		Direttore direttore = null;
		for(Direttore d : listaDirettori) {
			if(d.getCodiceFiscale().equals(codiceFiscale)) {
				direttore = d;
			}
		}
		return direttore;
	}
	public Officina getOfficinaByNome(String nome) {
		Officina officina = null;
		for(Officina o : listaOfficine) {
			if(o.getNome().equals(nome)) {
				officina = o;
			}
		}
		return officina;
	}
	public Dipendente getDipendenteByCF(String codiceFiscale) {
		Dipendente dipendente = null;
		for(Dipendente d : listaDipendenti) {
			if(d.getCodiceFiscale().equals(codiceFiscale)) {
				dipendente = d;
			}
		}
		return dipendente;
	}
	public Veicolo getVeicoloByTarga(String targa) {
		Veicolo veicolo = null;
		for(Veicolo v : listaVeicoli) {
			if(v.getTarga().equals(targa)) {
				veicolo = v;
			}
		}
		return veicolo;
	}
	public Fattura getFatturaByNumero(int numero) {
		Fattura fattura = null;
		for(Fattura f : listaFatture) {
			if (f.getNumero() == numero) {
				fattura = f;
			}
		}
		return fattura;
	}
	
	//TUTTI I METODI CON ADD NELLE VARIE LISTE
	public void addPersona(String codiceFiscale, String indirizzo, String telefono) {
		Persona p = new Persona(codiceFiscale, indirizzo, telefono);
		listaPersone.add(p);
	}
	public void addDirettore(String codiceFiscale, String indirizzo, String telefono, int eta) {
		Direttore d = new Direttore(codiceFiscale, indirizzo, telefono, eta);
		listaDirettori.add(d);
		listaPersone.add(d);
	}
	public void addDipendente(String codiceFiscale, String indirizzo, String telefono) {
		Dipendente d = new Dipendente(codiceFiscale, indirizzo, telefono);
		listaDipendenti.add(d);
		listaPersone.add(d);
	}
	public void addVeicolo(String modello, String tipo, String targa, int annoImmatricolazione, String codiceFiscalePersona) {
		Persona proprietario = getPersonaByCF(codiceFiscalePersona);
		Veicolo v = new Veicolo(modello, tipo, targa, annoImmatricolazione, proprietario);
		listaVeicoli.add(v);
	}
	public void addOfficina(String nome, String indirizzo, String codiceFiscaleDirettore) {
		Object direttore = getDirettoreByCF(codiceFiscaleDirettore);
		Officina o = new Officina(nome, indirizzo, (Direttore) direttore);
		listaOfficine.add(o);
	}
	public void addContrattoDirettore(String codiceFiscale, String nomeOfficina, String dataInizio, String dataFine) {
		Direttore direttore = getDirettoreByCF(codiceFiscale);
		Officina officina = getOfficinaByNome(nomeOfficina);
		
		Date dInizio = null;
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
	    try{
	        formatoData.setLenient(false);           
	        dInizio = formatoData.parse(dataInizio);
	        System.out.println("Data Inizio: " + formatoData.format(dInizio));
	    } catch (ParseException e) {
	           System.out.println("Formato data non valido.");
	    }
		
		if(dataFine == null) {
			ContrattoDirettore c = new ContrattoDirettore(direttore, officina, dInizio, null);
			listaContrattiDirettori.add(c);
		}else {		
			Date dFine = null;
		    try{
		        formatoData.setLenient(false);           
		        dFine = formatoData.parse(dataFine);
		        System.out.println("Data Fine: " + formatoData.format(dFine));
		    } catch (ParseException e) {
		           System.out.println("Formato data non valido.");
		    }
			ContrattoDirettore c = new ContrattoDirettore(direttore, officina, dInizio, dFine);
			listaContrattiDirettori.add(c);
		}
	}
	public void addContrattoDipendente(String codiceFiscaleDipendnete, String nomeOfficina, String dataInizio, String dataFine) {
		Dipendente dipendente = getDipendenteByCF(codiceFiscaleDipendnete);
		Officina officina = getOfficinaByNome(nomeOfficina);
		
		Date dInizio = null;
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
	    try{
	        formatoData.setLenient(false);           
	        dInizio = formatoData.parse(dataInizio);
	        System.out.println("Data Inizio: " + formatoData.format(dInizio));
	    } catch (ParseException e) {
	           System.out.println("Formato data non valido.");
	    }
		
		if(dataFine == null) {
			ContrattoDipendente c = new ContrattoDipendente(dipendente, officina, dInizio, null);
			listaContrattiDipendenti.add(c);
		}else {		
			Date dFine = null;
		    try{
		        formatoData.setLenient(false);           
		        dFine = formatoData.parse(dataFine);
		        System.out.println("Data Fine: " + formatoData.format(dFine));
		    } catch (ParseException e) {
		           System.out.println("Formato data non valido.");
		    }
		    ContrattoDipendente c = new ContrattoDipendente(dipendente, officina, dInizio, dFine);
		    listaContrattiDipendenti.add(c);
		}
	}
	public void addFattura(int numero, String dataRilascio, ArrayList<Dipendente> elencoDipendentiLavorato, String CFcompilatore, double costo) {
		Persona compilatore = getPersonaByCF(CFcompilatore);
		
		Date d = null;
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
		try{
			formatoData.setLenient(false);           
			d = formatoData.parse(dataRilascio);
			System.out.println("Data Fattura: " + formatoData.format(d));
		} catch (ParseException e) {
			System.out.println("Formato data non valido.");
		}
		
		Fattura f = new Fattura(numero, d, elencoDipendentiLavorato, compilatore, costo);
		listaFatture.add(f);
	}
	public void addRiparazione(int codiceRiparazione, String nomeOfficina, String targa, String dataAccettazione, int numeroFattura) {
		Veicolo veicolo = getVeicoloByTarga(targa);
		Officina officina = getOfficinaByNome(nomeOfficina);
		Fattura fattura = getFatturaByNumero(numeroFattura);
		
		Date d = null;
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
	    try{
	        formatoData.setLenient(false);           
	        d = formatoData.parse(dataAccettazione);
	        System.out.println("Data Riparazione: " + formatoData.format(d));
	    } catch (ParseException e) {
	           System.out.println("Formato data non valido.");
	    }
		
		Riparazione r = new Riparazione(codiceRiparazione, officina, veicolo, d, fattura);
		listaRiparazioni.add(r);
	}
	
	//OFFICINE SU CUI HA LAVORATO UN DIPENDENTE
	public ArrayList<Officina> officineLavoratoDipendente(String codiceFiscaleDiendente){
		ArrayList<Officina> elencoOfficine = new ArrayList<Officina>();
		for(ContrattoDipendente c : listaContrattiDipendenti) {
			if(c.getDipendente().getCodiceFiscale().equals(codiceFiscaleDiendente)) {
				elencoOfficine.add(c.getOfficina());
			}
		}
		return elencoOfficine;
	}
	//I VEICOLI CHE SONO STATI RIPARATI IN UN'OFFICINA
	public ArrayList<Veicolo> veicoliRiparatiInOfficina(String nomeOfficina){
		ArrayList<Veicolo> elencoVeicoli = new ArrayList<Veicolo>();
		for(Riparazione r : listaRiparazioni) {
			if(r.getOfficina().getNome().equals(nomeOfficina)) {
				elencoVeicoli.add(r.getVeicolo());
			}
		}
		return elencoVeicoli;
	}
	//DATO UN VEICOLO LE OFFICINE CHE LO HANNO RIPARATO
	public ArrayList<Officina> officineRiparatoVeicolo(String targa){
		ArrayList<Officina> elencoOfficine = new ArrayList<Officina>();
		for(Riparazione r : listaRiparazioni) {
			if(r.getVeicolo().getTarga().equals(targa)) {
				elencoOfficine.add(r.getOfficina());
			}
		}
		return elencoOfficine;
	}
	//QUANTO HA SPESO IN TOTALE UN PROPRIETARIO PER LE RIPARAZIONI
	public double totSpesaPropietario(String codiceFiscalePropietario) {
		double spesaIniziale = 0;
		for(Riparazione r : listaRiparazioni) {
			if(r.getVeicolo().getProprietario().getCodiceFiscale().equals(codiceFiscalePropietario)) {
				spesaIniziale += r.getFattura().getCosto();
			}
		}
		return spesaIniziale;
	}
	
	public boolean controlDuplicate(ArrayList<?> lista) {
		for(int i = 0; i<lista.size(); i++) {
			for(int j = 0; j<lista.size(); j++) {
				if(i != j && lista.get(i).toString().equals(lista.get(j).toString())) {
					return true;
				}
			}
		}
		return false;
	}

	public ArrayList<Dipendente> listaDuplicati(ArrayList<Dipendente> lista, Dipendente dipendente){
		ArrayList<Dipendente> duplicates = new ArrayList<Dipendente>();
		for(int i = 0; i<lista.size(); i++) {
			if(lista.get(i).getCodiceFiscale().equals(dipendente.getCodiceFiscale())) {
				duplicates.add(lista.get(i));
			}
		}
		return duplicates;
	}
	
	//ARRAYLIST DI ARRAYLIST
	public ArrayList<ArrayList<Dipendente>> listaDiListeDuplicati(ArrayList<Dipendente> lista){
		ArrayList<ArrayList<Dipendente>> listaDiListe = new ArrayList<ArrayList<Dipendente>>();
		
		HashSet<String> nonDuplicati = new HashSet<String>();
		for(Dipendente d : lista) {
			nonDuplicati.add(d.getCodiceFiscale());
		}
		
		for(String d : nonDuplicati) {
			Dipendente dipendente = getDipendenteByCF(d);
			if(listaDuplicati(lista, dipendente).size() > 1) {	
				listaDiListe.add(listaDuplicati(lista, dipendente));
			}
		}
		
		return listaDiListe;
	}

	//MERGE SORT
	//mettiamo in ordine le macchine per data di immatricolazione
	//creo un metodo per scomporre l'array
	public ArrayList<Veicolo> mergeSortAutoPerAnno(ArrayList<Veicolo> lista){
		int p = lista.size();
		//imposto la condizione che se la lista contiene un elemento me la ritorni e finisce 
		//il metodo
		if(p < 2) {
			return lista;
		}
		//trovo il punto medio della lista e la divii�do in due liste
		int mid = p/2;
		ArrayList<Veicolo> listaSinistra = new ArrayList<Veicolo>();
		ArrayList<Veicolo> listaDestra = new ArrayList<Veicolo>();
		for(int i = 0; i < mid; i++) {
			listaSinistra.add(lista.get(i));
		}
		for(int i = mid; i < p; i++) {
			listaDestra.add(lista.get(i));
		}
		//richiamo il metodo creando un metodo ricorsino che si interrompe con la condizione 
		//all'inizio quando la lista � minore di 2
		mergeSortAutoPerAnno(listaSinistra);
		mergeSortAutoPerAnno(listaDestra);
		
		//creo un array list ordinato con dentro i vari set fatti nel metodo magreAutoPerAnno
		//e me lo faccio ritornare ogni volta  fino a quando non � al completo
	 	ArrayList<Veicolo> listaOrdinata = new ArrayList<Veicolo>();
	 	listaOrdinata = margeAutoPerAnno(lista, listaSinistra, listaDestra);
	 	return listaOrdinata;
	}
	
	//creo il metodo mergeAutoPerAnno da usare in margeSortAutoPerAnno
	public ArrayList<Veicolo> margeAutoPerAnno(ArrayList<Veicolo> lista, ArrayList<Veicolo> listaSinistra, ArrayList<Veicolo> listaDestra ) {
		//inizio con lo stabilire la grandezza dell'array di sisnistra e quello di destra
		int ss = listaSinistra.size();
		int sd = listaDestra.size();
		
		//creo dei puntatori i per l'array di sinistra, j per l'array di destra 
		//e k per la lista che passo
		int i = 0;
		int j = 0;
		int k = 0;
		
		/**
		 * creo un cliclo while che deve girare fino a quando o la lista di destra o quella 
		 * di sinistra non contiene pi� elementi, in questo ciclo creo la condizione per cui
		 * se il primo numero della lista di sinistra � minore dee primo di destra, allora
		 * nella posizione designata k della lista pincipale inserisco il valore minore e 
		 * passo al prossimo valore 
		 * della lista di sinistra, idem nel caso si verifichi che il valore minore sia 
		 * quello di destra, alla fine di ogni selezione passo alla posizione successiva
		 * della lista principale k++
		 */
		
		while(i < ss && j < sd) {
			if(listaSinistra.get(i).getAnnoImmatricolazione() <= listaDestra.get(j).getAnnoImmatricolazione()) {
				lista.set(k, listaSinistra.get(i));
				i++;
			}else {
				lista.set(k,listaDestra.get(j));
				j++;
			}
			k++;
		}
		
		/**
		 * una volta fatto ci� ed interrotto il primo ciclo while controllo quale delle due
		 * liste si sia azzerata con due cicli while, uno che interviene nel caso 
		 * ci siano ancora elementi nella lista di sinistra e uno di destra, si attiver�
		 * in automatico quello necessario e servira a inserire nelle posizioni successive
		 * i numeri rimanenti di una lista in quella principale secondo l'ordine in lista
		 */
		while(i < ss) {
			lista.set(k, listaSinistra.get(i));		
			i++;
			k++;
		}
		while(j < sd) {
			lista.set(k, listaDestra.get(j));		
			j++;
			k++;
		}
		
		//finito il tutto mi ritorner� la listaprincipale (passata nel metodo) ordinata
		return lista;
	}
	
	
	//BUBBLE SORT
	public ArrayList<Veicolo> bubbleSortAutoPerAnno(ArrayList<Veicolo> lista){
		//ciclo la lista che passo nel metodo
		for(int i = 0; i < lista.size(); i++) {
			//creo uno stop per il prossimo cicloche se rimane invariato nel corso del 
			//metodo me lo ferma dandomi l'array ordinato
			boolean stop = true;
			//ciclo nuovamente la lista passata nel metodo
			for(int j = 0; j < lista.size() -1 ;j++) {
				//creo un int che salvi la posizione successiva a quella che sto ciclando
				int p = j+1;
				/**
				 * controllo se l'oggetto che sto ciclando in questo momento e maggiore 
				 * del successivo, se succede allora salvo questo oggetto in k, sposto
				 * l'oggetto successivo alla posizione corrente con set, e
				 * sempre con set sposto l'oggetto corrente alla posizione successiva
				 */
				if((lista.get(j).getAnnoImmatricolazione()) > (lista.get(p).getAnnoImmatricolazione())) {
					Veicolo k = lista.get(j);
					lista.set(j, lista.get(p));
					lista.set(p, k);
					//se tutto questo avviene vuol dire si sono mossi degli elementi, quindi
					//la lista non � ordinata e bisogna andare avanti,
					//quindi cambio il booleano per non far interrompere il ciclo
					stop = false;
				}
			}
			if(stop) {
				//se non avviene il cambio boolenao vuol dire che la lista � ordinata
				//quindi mi fermo e restituisco  la lista ordinata
				break;
			}
			
		}
		return lista;
	}
	
	//RESTITUIRE N STANZE  PER OGNI OGGETTO
}
