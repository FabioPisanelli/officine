package view;

import java.util.ArrayList;
import controller.ControllerOfficine;
import model.ContrattoDipendente;
import model.Dipendente;

public class Home {

	public static void main(String[] args) {
		
		ControllerOfficine co = new ControllerOfficine();
		
		//CREO DEI DIRETTORI E LI AGGIUNGO ALLA LISTA
		co.addDirettore("GDFTSH87A98J987U", "Torino, via quella n�12", "3568741254", 44);
		co.addDirettore("HDTBCG58D85H215G", "Torino, via sappilo n�99", "6583214569", 35);
		co.addDirettore("YRTLTF87E63T354A", "Asti, strada mai n�45", "9986632541", 55);
				
		//CREO DEI DIPENDENTI E LI AGGIUNGO ALLA LISTA
		co.addDipendente("DGDATE65T37Y763F", "Torino, via mannaia n�7b", "6586862154");
		co.addDipendente("GTRHYE34T35T647G", "Torino, via sorpesa  n�98", "3256987412");
		co.addDipendente("KLAUDH34H76Y758F", "Torino, via storta  n�22", "8896523654");
		co.addDipendente("BCHDHD55L88D847D", "Torino, strada buia n�77", "2223334587");
		co.addDipendente("QWUHSV37S11S987W", "Pinerolo, via beata n�3a", "1223364450");
		co.addDipendente("JUFLLP25F88E698Q", "Asti, corso affollato n�55", "8796541258");
		co.addDipendente("KLAUDH34H76Y758F", "Torino, via storta  n�22", "8896523654");
		co.addDipendente("JJSYRU38K99I392O", "Torino, corso giovani n�71b", "4589632378");
		co.addDipendente("JJSYRU38K99I392O", "Torino, corso giovani n�71b", "4589632378");
		co.addDipendente("ASWRET77I87I362P", "Torino, strada bagnata n�8", "1152365897");
		co.addDipendente("KLAUDH34H76Y758F", "Torino, via storta  n�22", "8896523654");
		co.addDipendente("HETDTE22I95D342S", "Torino, via sempre quella n�64", "9987785588");
		
		//CREO DELLE PERSONE E LE AGGIUNGO ALLA LISTA
		co.addPersona("HDYBDG36T36Y354T", "Torino, via losca n�10", "2548786699");
		co.addPersona("LQODFR47Y47H930F", "Torino, via marte n�47c", "3658741254");
		co.addPersona("SWERTD45T64U847Y", "Moncalieri, strada torino n�23", "9874563321");
		co.addPersona("LLODEH43E45D576H", "Torino, via zaino n�61", "7896642154");
		co.addPersona("PRVJDE44T55J435I", "Torino, via rossi n�15", "1245884587");
		co.addPersona("NNSMSS32M45D456I", "Torino, via lessi n�41", "2547896256");
		
		//CREO DEI VEICOLI E LI AGGIUNGO NELLA LISTA
		co.addVeicolo("giulietta", "alfa romeo", "JH234TD", 2018, "HDYBDG36T36Y354T");
		co.addVeicolo("a180", "mercedes", "KI376DF", 2016, "LQODFR47Y47H930F");
		co.addVeicolo("clio", "renault", "SU375TR", 2002, "SWERTD45T64U847Y");
		co.addVeicolo("120d", "BMW", "ZX746GF", 2018, "LLODEH43E45D576H");
		co.addVeicolo("punto", "fiat", "PP284UT", 2012, "PRVJDE44T55J435I");
		co.addVeicolo("giulia", "alfa romeo", "OI938TU", 2012, "NNSMSS32M45D456I");
		co.addVeicolo("120d", "BMW", "ZX746GF", 2018, "LLODEH43E45D576H");
		
		//CREO DELLE OFFICINE E LE AGGIUNGO NELLA LISTA
		co.addOfficina("Ripariamo tutto", "Torino, via roma n�5", "GDFTSH87A98J987U");
		co.addOfficina("Auto informa", "Torino, via nizza n�24", "HDTBCG58D85H215G");
		co.addOfficina("Autoville", "Torino, via narnia n�67e", "YRTLTF87E63T354A");

		//CREO I CONTRATTI DEI DIRETTORI E LI AGGIUNGO IN LISTA
		co.addContrattoDirettore("GDFTSH87A98J987U", "Ripariamo tutto", "03/01/2022", null);
		co.addContrattoDirettore("HDTBCG58D85H215G", "Auto informa", "25/06/2021", null);
		co.addContrattoDirettore("YRTLTF87E63T354A", "Autoville", "08/11/2019", "08/11/2030");
		
		//CREO I CONTRATTI DEI DIPENDNETI E LI AGGIUNGO IN LISTA
		co.addContrattoDipendente("DGDATE65T37Y763F", "Ripariamo tutto", "03/01/2022", null);
		co.addContrattoDipendente("GTRHYE34T35T647G", "Ripariamo tutto", "03/01/2022", "03/06/2022");
		co.addContrattoDipendente("KLAUDH34H76Y758F", "Ripariamo tutto", "03/01/2022", null);
		co.addContrattoDipendente("BCHDHD55L88D847D", "Auto informa", "30/06/2021", null);
		co.addContrattoDipendente("QWUHSV37S11S987W", "Auto informa", "5/10/2021", null);
		co.addContrattoDipendente("JUFLLP25F88E698Q", "Auto informa", "14/09/2021", null);
		co.addContrattoDipendente("JJSYRU38K99I392O", "Autoville", "08/11/2019", "08/11/2024");
		co.addContrattoDipendente("ASWRET77I87I362P", "Autoville", "15/11/2019", null);
		co.addContrattoDipendente("HETDTE22I95D342S", "Autoville", "15/12/2019", null);
		
		//CREO LE FATTURE E LE AGGIUNGO IN LISTA
		ArrayList<Dipendente> elencoDipendentiLavorato1 = new ArrayList<Dipendente>();
		for(ContrattoDipendente c : co.getAllContrattiDipendenti()){
			if(c.getOfficina().getNome().equals("Ripariamo tutto")) {
				elencoDipendentiLavorato1.add(c.getDipendente());
			}
		}
		co.addFattura(1, "15/01/2022", elencoDipendentiLavorato1, "GDFTSH87A98J987U", 1523.25);
		co.addFattura(2, "25/01/2022", elencoDipendentiLavorato1, "KLAUDH34H76Y758F", 254.18);
		
		ArrayList<Dipendente> elencoDipendentiLavorato2 = new ArrayList<Dipendente>();
		for(ContrattoDipendente c : co.getAllContrattiDipendenti()){
			if(c.getOfficina().getNome().equals("Auto informa")) {
				elencoDipendentiLavorato2.add(c.getDipendente());
			}
		}
		co.addFattura(3, "30/10/2021", elencoDipendentiLavorato2, "QWUHSV37S11S987W", 784.24);
		co.addFattura(4, "15/11/2021", elencoDipendentiLavorato2, "HDTBCG58D85H215G", 2487.32);
		
		ArrayList<Dipendente> elencoDipendentiLavorato3 = new ArrayList<Dipendente>();
		for(ContrattoDipendente c : co.getAllContrattiDipendenti()){
			if(c.getOfficina().getNome().equals("Autoville")) {
				elencoDipendentiLavorato3.add(c.getDipendente());
			}
		}
		co.addFattura(5, "25/11/2021", elencoDipendentiLavorato3, "ASWRET77I87I362P", 987.89);
		co.addFattura(6, "15/12/2021", elencoDipendentiLavorato3, "ASWRET77I87I362P", 1125.33);
		
		//CREO LE RIPARAZIONI E LE METTO IN LISTA
		co.addRiparazione(125, "Ripariamo tutto", "ZX746GF", "12/01/2022", 1);
		co.addRiparazione(547, "Ripariamo tutto", "KI376DF", "20/01/2022", 2);
		co.addRiparazione(32, "Auto informa", "SU375TR", "15/10/2021", 3);
		co.addRiparazione(485, "Auto informa", "OI938TU", "4/11/2021", 4);
		co.addRiparazione(25, "Autoville", "PP284UT", "6/11/2021", 5);
		co.addRiparazione(77, "Autoville", "JH234TD", "9/12/2021", 6);
		//-----------------------------------------------------------------------//
		//-----------------------------------------------------------------------//
		//-----------------------------------------------------------------------//

		co.officineLavoratoDipendente("DGDATE65T37Y763F");
		co.veicoliRiparatiInOfficina("Autoville");
		co.officineRiparatoVeicolo("OI938TU");
		co.totSpesaPropietario("HDYBDG36T36Y354T");
		co.officineLavoratoDipendente("QWUHSV37S11S987W");
		co.listaDuplicati(co.getAllDipendenti(), co.getAllDipendenti().get(2));
		co.listaDiListeDuplicati(co.getAllDipendenti());
		co.mergeSortAutoPerAnno(co.getAllVeicoli());
		co.bubbleSortAutoPerAnno(co.getAllVeicoli());
		System.out.println(co.controlDuplicate(co.getAllDipendenti()));
		
		
		
		
	}

}
